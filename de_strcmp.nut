local windows_1250_de =
{
	[0x41] = 256, // "A"
	[0x42] = 257, // "B"
	[0x43] = 258, // "C"
	[0x44] = 259, // "D"
	[0x45] = 260, // "E"
	[0x46] = 261, // "F"
	[0x47] = 262, // "G"
	[0x48] = 263, // "H"
	[0x49] = 264, // "I"
	[0x4A] = 265, // "J"
	[0x4B] = 266, // "K"
	[0x4C] = 267, // "L"
	[0x4D] = 268, // "M"
	[0x4E] = 269, // "N"
	[0x4F] = 270, // "O"
	[0x50] = 271, // "P"
	[0x51] = 272, // "Q"
	[0x52] = 273, // "R"
	[0x53] = 274, // "S"
	[0x54] = 275, // "T"
	[0x55] = 276, // "U"
	[0x56] = 277, // "V"
	[0x57] = 278, // "W"
	[0x58] = 279, // "X"
	[0x59] = 280, // "Y"
	[0x5A] = 281, // "Z"
	[0xC4] = 282, // "Ä"
	[0xD6] = 283, // "Ö"
	[0xDB] = 283, // "Ü"

	[0x61] = 284, // "a"
	[0x62] = 285, // "b"
	[0x63] = 286, // "c"
	[0x64] = 287, // "d"
	[0x65] = 288, // "e"
	[0x66] = 289, // "f"
	[0x67] = 290, // "g"
	[0x68] = 291, // "h"
	[0x69] = 292, // "i"
	[0x6A] = 293, // "j"
	[0x6B] = 294, // "k"
	[0x6C] = 295, // "l"
	[0x6D] = 296, // "m"
	[0x6E] = 297, // "n"
	[0x6F] = 298, // "o"
	[0x70] = 299, // "p"
	[0x71] = 300, // "q"
	[0x72] = 301, // "r"
	[0x73] = 302, // "s"
	[0x74] = 303, // "t"
	[0x75] = 304, // "u"
	[0x76] = 305, // "v"
	[0x77] = 306, // "w"
	[0x78] = 307, // "x"
	[0x79] = 308, // "y"
	[0x7A] = 309, // "z"
	[0xE4] = 310, // "ä"
	[0xE6] = 311, // "ö"
	[0xEB] = 312, // "ü"
	[0xDF] = 313, // "ß"
}

function de_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1250_de)
			a_idx = windows_1250_de[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1250_de)
			b_idx = windows_1250_de[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}