local windows_1251_ru =
{
	[0xC0] = 256, // "А"
	[0xC1] = 257, // "Б"
	[0xC2] = 258, // "В"
	[0xC3] = 259, // "Г"
	[0xC4] = 260, // "Д"
	[0xC5] = 261, // "Е"
	[0xA8] = 262, // "Ё"
	[0xC6] = 263, // "Ж"
	[0xC7] = 264, // "З"
	[0xC8] = 265, // "И"
	[0xC9] = 266, // "Й"
	[0xCA] = 267, // "К"
	[0xCB] = 268, // "Л"
	[0xCC] = 269, // "М"
	[0xCD] = 270, // "Н"
	[0xCE] = 271, // "О"
	[0xCF] = 272, // "П"
	[0xD0] = 273, // "Р"
	[0xD1] = 274, // "С"
	[0xD2] = 275, // "Т"
	[0xD3] = 276, // "У"
	[0xD4] = 277, // "Ф"
	[0xD5] = 278, // "Х"
	[0xD6] = 279, // "Ц"
	[0xD7] = 280, // "Ч"
	[0xD8] = 281, // "Ш"
	[0xD9] = 282, // "Щ"
	[0xDA] = 283, // "Ъ"
	[0xDB] = 284, // "Ы"
	[0xDC] = 285, // "Ь"
	[0xDD] = 286, // "Э"
	[0xDE] = 287, // "Ю"
	[0xDF] = 288, // "Я"

	[0xE0] = 289, // "а"
	[0xE1] = 290, // "б"
	[0xE2] = 291, // "в"
	[0xE3] = 292, // "г"
	[0xE4] = 293, // "д"
	[0xE5] = 294, // "е"
	[0xB8] = 295, // "ё"
	[0xE6] = 296, // "ж"
	[0xE7] = 297, // "з"
	[0xE8] = 298, // "и"
	[0xE9] = 299, // "й"
	[0xEA] = 300, // "к"
	[0xEB] = 301, // "л"
	[0xEC] = 302, // "м"
	[0xED] = 303, // "н"
	[0xEE] = 304, // "о"
	[0xEF] = 305, // "п"
	[0xF0] = 306, // "р"
	[0xF1] = 307, // "с"
	[0xF2] = 308, // "т"
	[0xF3] = 309, // "у"
	[0xF4] = 310, // "ф"
	[0xF5] = 311, // "х"
	[0xF6] = 312, // "ц"
	[0xF7] = 313, // "ч"
	[0xF8] = 314, // "ш"
	[0xF9] = 315, // "щ"
	[0xFA] = 316, // "ъ"
	[0xFB] = 317, // "ы"
	[0xFC] = 318, // "ь"
	[0xFD] = 319, // "э"
	[0xFE] = 320, // "ю"
	[0xFF] = 321, // "я"
}

function ru_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1251_ru)
			a_idx = windows_1251_ru[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1251_ru)
			b_idx = windows_1251_ru[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}