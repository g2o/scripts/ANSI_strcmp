local windows_1252_es =
{
	[0x41] = 256, // "A"
	[0x42] = 257, // "B"
	[0x43] = 258, // "C"
	[0x44] = 259, // "D"
	[0x45] = 260, // "E"
	[0x46] = 261, // "F"
	[0x47] = 262, // "G"
	[0x48] = 263, // "H"
	[0x49] = 264, // "I"
	[0x4A] = 265, // "J"
	[0x4B] = 266, // "K"
	[0x4C] = 267, // "L"
	[0x4D] = 268, // "M"
	[0x4E] = 269, // "N"
	[0xD1] = 270, // "Ñ"
	[0x4F] = 271, // "O"
	[0x50] = 272, // "P"
	[0x51] = 273, // "Q"
	[0x52] = 274, // "R"
	[0x53] = 275, // "S"
	[0x54] = 276, // "T"
	[0x55] = 277, // "U"
	[0x56] = 278, // "V"
	[0x57] = 279, // "W"
	[0x58] = 280, // "X"
	[0x59] = 281, // "Y"
	[0x5A] = 282, // "Z"

	[0x61] = 283, // "a"
	[0x62] = 284, // "b"
	[0x63] = 285, // "c"
	[0x64] = 286, // "d"
	[0x65] = 287, // "e"
	[0x66] = 288, // "f"
	[0x67] = 289, // "g"
	[0x68] = 290, // "h"
	[0x69] = 291, // "i"
	[0x6A] = 292, // "j"
	[0x6B] = 293, // "k"
	[0x6C] = 294, // "l"
	[0x6D] = 295, // "m"
	[0x6E] = 296, // "n"
	[0x6E] = 297, // "ñ"
	[0x6F] = 298, // "o"
	[0x70] = 299, // "p"
	[0x71] = 300, // "q"
	[0x72] = 301, // "r"
	[0x73] = 302, // "s"
	[0x74] = 303, // "t"
	[0x75] = 304, // "u"
	[0x76] = 305, // "v"
	[0x77] = 306, // "w"
	[0x78] = 307, // "x"
	[0x79] = 308, // "y"
	[0x7A] = 309, // "z"
}

function es_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1252_es)
			a_idx = windows_1252_es[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1252_es)
			b_idx = windows_1252_es[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}