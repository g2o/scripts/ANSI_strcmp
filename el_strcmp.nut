local windows_1253_el =
{
	[0xC1] = 256, // "Α"
	[0xC2] = 257, // "Β"
	[0xC3] = 258, // "Γ"
	[0xC4] = 259, // "Δ"
	[0xC5] = 260, // "Ε"
	[0xC6] = 261, // "Ζ"
	[0xC7] = 262, // "Η"
	[0xC8] = 263, // "Θ"
	[0xC9] = 264, // "Ι"
	[0xCA] = 265, // "Κ"
	[0xCB] = 266, // "Λ"
	[0xCC] = 267, // "Μ"
	[0xCD] = 268, // "Ν"
	[0xCE] = 269, // "Ξ"
	[0xCF] = 270, // "Ο"
	[0xD0] = 271, // "Π"
	[0xD1] = 272, // "Ρ"
	[0xD3] = 273, // "Σ"
	[0xD4] = 274, // "Τ"
	[0xD5] = 275, // "Υ"
	[0xD6] = 276, // "Φ"
	[0xD7] = 277, // "Χ"
	[0xD8] = 278, // "Ψ"
	[0xD9] = 279, // "Ω"

	[0xE1] = 280, // "α"
	[0xE2] = 281, // "β"
	[0xE3] = 282, // "γ"
	[0xE4] = 283, // "δ"
	[0xE5] = 284, // "ε"
	[0xE6] = 285, // "ζ"
	[0xE7] = 286, // "η"
	[0xE8] = 287, // "θ"
	[0xE9] = 288, // "ι"
	[0xEA] = 289, // "κ"
	[0xEB] = 290, // "λ"
	[0xEC] = 291, // "μ"
	[0xED] = 292, // "ν"
	[0xEE] = 293, // "ξ"
	[0xEF] = 294, // "ο"
	[0xF0] = 295, // "π"
	[0xF1] = 296, // "ρ"
	[0xF3] = 297, // "σ"
	[0xF2] = 298, // "ς"
	[0xF4] = 299, // "τ"
	[0xF5] = 300, // "υ"
	[0xF6] = 301, // "φ"
	[0xF7] = 302, // "χ"
	[0xF8] = 303, // "ψ"
	[0xF9] = 304, // "ω"
}

function el_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1253_el)
			a_idx = windows_1253_el[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1253_el)
			b_idx = windows_1253_el[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}