local windows_1250_pl =
{
	[0x41] = 256, // "A"
	[0xA5] = 257, // "Ą"
	[0x42] = 258, // "B"
	[0x43] = 259, // "C"
	[0xC6] = 260, // "Ć"
	[0x44] = 261, // "D"
	[0x45] = 262, // "E"
	[0xCA] = 263, // "Ę"
	[0x46] = 264, // "F"
	[0x47] = 265, // "G"
	[0x48] = 266, // "H"
	[0x49] = 267, // "I"
	[0x4A] = 268, // "J"
	[0x4B] = 269, // "K"
	[0x4C] = 270, // "L"
	[0xA3] = 271, // "Ł"
	[0x4D] = 272, // "M"
	[0x4E] = 273, // "N"
	[0xD1] = 274, // "Ń"
	[0x4F] = 275, // "O"
	[0xD3] = 276, // "Ó"
	[0x50] = 277, // "P"
	[0x51] = 278, // "Q"
	[0x52] = 279, // "R"
	[0x53] = 280, // "S"
	[0x8C] = 281, // "Ś"
	[0x54] = 282, // "T"
	[0x55] = 283, // "U"
	[0x56] = 284, // "V"
	[0x57] = 285, // "W"
	[0x58] = 286, // "X"
	[0x59] = 287, // "Y"
	[0x5A] = 288, // "Z"
	[0x8F] = 289, // "Ź"
	[0xAF] = 290, // "Ż"

	[0x61] = 291, // "a"
	[0xB9] = 292, // "ą"
	[0x62] = 293, // "b"
	[0x63] = 294, // "c"
	[0xE6] = 295, // "ć"
	[0x64] = 296, // "d"
	[0x65] = 297, // "e"
	[0xEA] = 298, // "ę"
	[0x66] = 299, // "f"
	[0x67] = 300, // "g"
	[0x68] = 301, // "h"
	[0x69] = 302, // "i"
	[0x6A] = 303, // "j"
	[0x6B] = 304, // "k"
	[0x6C] = 305, // "l"
	[0xB3] = 306, // "ł"
	[0x6D] = 307, // "m"
	[0x6E] = 308, // "n"
	[0xF1] = 309, // "ń"
	[0x6F] = 310, // "o"
	[0xF3] = 311, // "ó"
	[0x70] = 312, // "p"
	[0x71] = 313, // "q"
	[0x72] = 314, // "r"
	[0x73] = 315, // "s"
	[0x9C] = 316, // "ś"
	[0x74] = 317, // "t"
	[0x75] = 318, // "u"
	[0x76] = 319, // "v"
	[0x77] = 320, // "w"
	[0x78] = 321, // "x"
	[0x79] = 322, // "y"
	[0x7A] = 323, // "z"
	[0x9F] = 324, // "ź"
	[0xBF] = 325, // "ż"
}

function pl_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1250_pl)
			a_idx = windows_1250_pl[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1250_pl)
			b_idx = windows_1250_pl[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}