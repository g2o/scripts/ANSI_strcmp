# Introduction

**ANSI_strcmp** is a **shared** squirrel scripts package that allows you to compare strings that are using **windows-12xx** encodings.  
It works by implementing a custom lookup table that contains alphabet for specific language.  
The conversion time complexity can be represented as *o(n)*.

Example usages when this can be used:
- Comparing which ANSI string should be displayed first (alphabetically)
- Sorting ANSI strings alphabetically 

You should only use this script when you need to properly sort/compare ANSI strings that you can't assign manually, like: player name, etc.

## Contributing

Any merge request is welcome.  
I encourage anyone that want to extend this script to create the script array strcmp routines for their prefereable windows encoding page,  
or fix the order of letters in specific script for their language.  
To create map conversions i've used [this wikipedia article](https://en.wikipedia.org/wiki/Windows-1250) and i've put the letters in my mapped array  
in alphabetic order, so that i can just compare two numbers together to determine which one has lower/higher value.

## Script(s) naming convention

- windows12xx_strcmp.nut
    
    This script provides the function to compare **ANSI** strings.  
    It's saved with `UTF-8` encoding just to be able to properly display the character comments.


## How to install?

1.Clone or download the repository source code  
2.Extract the code whenether you like in your g2o_server directory  
3.Add this line to your XML loading section  

**NOTE** That you have to edit the example paths to match with your script location.  
**NOTE** That you may want to include the script(s) only for one specific side.

```xml
<script src="path/to/script/pl_strcmp.nut" side="shared" />
```

## Usage example

### Example script demonstrating polish words being sorted in ASCENDING order

```js
addEventHandler("onInit", function()
{
    local strings = [
        "ćma"
        "żołądź",
        "bałwan",
        "źdźbło",
    ]

    strings.sort(@(a, b) pl_strcmp(a, b))
    foreach (i, v in strings)
        print(v)
})
```
