local windows_1251_ua =
{
	[0xC0] = 256, // "А"
	[0xC1] = 257, // "Б"
	[0xC2] = 258, // "В"
	[0xC3] = 259, // "Г"
	[0xA5] = 260, // "Ґ"
	[0xC4] = 261, // "Д"
	[0xC5] = 262, // "Е"
	[0xAA] = 263, // "Є"
	[0xC6] = 264, // "Ж"
	[0xC7] = 265, // "З"
	[0xC8] = 266, // "И"
	[0xB2] = 267, // "І"
	[0xAF] = 268, // "Ї"
	[0xC9] = 269, // "Й"
	[0xCA] = 270, // "К"
	[0xCB] = 271, // "Л"
	[0xCC] = 272, // "М"
	[0xCD] = 273, // "Н"
	[0xCE] = 274, // "О"
	[0xCF] = 275, // "П"
	[0xD0] = 276, // "Р"
	[0xD1] = 277, // "С"
	[0xD2] = 278, // "Т"
	[0xD3] = 279, // "У"
	[0xD4] = 280, // "Ф"
	[0xD5] = 281, // "Х"
	[0xD6] = 282, // "Ц"
	[0xD7] = 283, // "Ч"
	[0xD8] = 284, // "Ш"
	[0xD9] = 285, // "Щ"
	[0xDC] = 286, // "Ь"
	[0xDE] = 287, // "Ю"
	[0xDF] = 288, // "Я"

	[0xE0] = 289, // "а"
	[0xE1] = 290, // "б"
	[0xE2] = 291, // "в"
	[0xE3] = 292, // "г"
	[0xB4] = 293, // "ґ"
	[0xE4] = 294, // "д"
	[0xE5] = 295, // "е"
	[0xBA] = 296, // "є"
	[0xE6] = 297, // "ж"
	[0xE7] = 298, // "з"
	[0xE8] = 299, // "и"
	[0xB3] = 300, // "і"
	[0xBF] = 301, // "ї"
	[0xE9] = 302, // "й"
	[0xEA] = 303, // "к"
	[0xEB] = 304, // "л"
	[0xEC] = 305, // "м"
	[0xED] = 306, // "н"
	[0xEE] = 307, // "о"
	[0xEF] = 308, // "п"
	[0xF0] = 309, // "р"
	[0xF1] = 310, // "с"
	[0xF2] = 311, // "т"
	[0xF3] = 312, // "у"
	[0xF4] = 313, // "ф"
	[0xF5] = 314, // "х"
	[0xF6] = 315, // "ц"
	[0xF7] = 316, // "ч"
	[0xF8] = 317, // "ш"
	[0xF9] = 318, // "щ"
	[0xFC] = 319, // "ь"
	[0xFE] = 320, // "ю"
	[0xFF] = 321, // "я"
}

function ua_strcmp(a, b)
{
	local a_length = a.len()
	local b_length = b.len()

	local min_length = (a_length < b_length) ? a_length : b_length

	for (local i = 0; i < min_length; ++i)
	{
		local a_idx = 256 + a[i]
		if (a_idx in windows_1251_ua)
			a_idx = windows_1251_ua[a_idx]

		local b_idx = 256 + b[i]
		if (b_idx in windows_1251_ua)
			b_idx = windows_1251_ua[b_idx]

		if (a_idx == b_idx)
			continue

		if (a_idx < b_idx)
			return -1

		if (a_idx > b_idx)
			return 1
	}

	if (a_length < b_length)
		return -1

	if (b_length > a_length)
		return 1

	return 0
}